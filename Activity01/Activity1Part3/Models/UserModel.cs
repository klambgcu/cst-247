﻿/*
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2020-10-05
 * Class     : CST-247 Enterprise Computer Programming III
 * Professor : Dinesh Sthapit M.S.
 * Assignment: Activity 1-3
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * Login authentication: Create SQL table and validate user entry
 * ---------------------------------------------------------------
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Activity1Part3.Models
{
    public class UserModel
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}