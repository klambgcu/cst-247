﻿/*
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2020-10-05
 * Class     : CST-247 Enterprise Computer Programming III
 * Professor : Dinesh Sthapit M.S.
 * Assignment: Activity 1-3
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * Login authentication: Create SQL table and validate user entry
 * ---------------------------------------------------------------
 */

using Activity1Part3.Models;
using Activity1Part3.Services.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Activity1Part3.Controllers
{
    public class LoginController : Controller
    {
        // GET: Login
        [HttpGet]
        public ActionResult Index()
        {
            return View("Login");
        }
        
        [HttpPost]
        public ActionResult Login(UserModel model)
        {
            SecurityService service = new SecurityService();
            bool results = service.Authenticate(model);

            if (results)
                return View("LoginPassed", model);
            else
                return View("LoginFailed", model);
        }
    
    
    }
}