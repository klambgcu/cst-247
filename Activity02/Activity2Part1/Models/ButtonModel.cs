﻿/*
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2020-10-15
 * Class     : CST-247 Enterprise Computer Programming III
 * Professor : Dinesh Sthapit M.S.
 * Assignment: Activity 2 Multiple Sections
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * Part 1: Introduction to .NET Razor
 * Part 2: Advanced .NET Razor
 * Part 3: Data Validation
 * ---------------------------------------------------------------
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Activity2Part1.Models
{
    public class ButtonModel
    {
        public ButtonModel(bool State)
        {
            this.State = State;
        }

        public bool State { get; set; }
    }
}