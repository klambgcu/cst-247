﻿/*
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2020-10-15
 * Class     : CST-247 Enterprise Computer Programming III
 * Professor : Dinesh Sthapit M.S.
 * Assignment: Activity 2 Multiple Sections
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * Part 1: Introduction to .NET Razor
 * Part 2: Advanced .NET Razor
 * Part 3: Data Validation
 * ---------------------------------------------------------------
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Activity2Part1
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Button",
                url: "{controller}",
                defaults: new { controller = "Button", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Test",
                url: "{controller}",
                defaults: new { controller = "Test", action = "Index", id = UrlParameter.Optional }
            );


            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
