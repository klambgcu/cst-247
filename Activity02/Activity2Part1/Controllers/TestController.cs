﻿/*
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2020-10-15
 * Class     : CST-247 Enterprise Computer Programming III
 * Professor : Dinesh Sthapit M.S.
 * Assignment: Activity 2 Multiple Sections
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * Part 1: Introduction to .NET Razor
 * Part 2: Advanced .NET Razor
 * Part 3: Data Validation
 * ---------------------------------------------------------------
 */

using Activity2Part1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Activity2Part1.Controllers
{
    public class TestController : Controller
    {
        // GET: Test
        public ActionResult Index()
        {
            List<UserModel> users = new List<UserModel>
            {
                new UserModel("Mickey Mouse", "Mickey@Disney.com", "123-456-7890"),
                new UserModel("Minnie Mouse", "Minnie@Disney.com", "321-321-9876"),
                new UserModel("Donald Duck", "Donald@Disney.com", "222-333-4444"),
                new UserModel("Daisy Duck", "Daisy@Disney.com", "421-776-3232"),
                new UserModel("Clarabelle Cow", "Clarabelle@Disney.com", "422-788-9921"),
                new UserModel("Oswald Rabbit", "Oswald@Disney.com", "480-886-7754")
            };
            return View("Test", users);
        }
    }
}