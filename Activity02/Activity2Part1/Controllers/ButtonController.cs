﻿/*
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2020-10-15
 * Class     : CST-247 Enterprise Computer Programming III
 * Professor : Dinesh Sthapit M.S.
 * Assignment: Activity 2 Multiple Sections
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * Part 1: Introduction to .NET Razor
 * Part 2: Advanced .NET Razor
 * Part 3: Data Validation
 * ---------------------------------------------------------------
 */

using Activity2Part1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Activity2Part1.Controllers
{
    public class ButtonController : Controller
    {
        private static List<ButtonModel> buttons = new List<ButtonModel>();
        // GET: Button
        public ActionResult Index()
        {
            buttons.Add(new ButtonModel(true));
            buttons.Add(new ButtonModel(true));

            return View("Button", buttons);
        }

        public ActionResult OnButtonClick(string mine)
        {
            if (mine.Equals("1")) buttons[0].State = !buttons[0].State;
            else if (mine.Equals("2")) buttons[1].State = !buttons[1].State;

            return View("Button", buttons);
        }
    }
}