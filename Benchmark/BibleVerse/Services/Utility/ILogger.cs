﻿/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2020-11-16
 * Class     : CST-247 Enterprise Computer Programming III
 * Professor : Dinesh Staphit
 * Assignment: Benchmark - BibleVerse
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Create Database to store: Testament, Books, Chapters, Verses
 * 2. Create UI to allow entry and search for verses
 * 3. Use NLog for logging
 * 4. Use Unity for IoC
 * 5. Implement DAO design pattern
 * ---------------------------------------------------------------
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BibleVerse.Services.Utility
{
    // Facade design pattern to add abstraction layer
    // for logging. Should we need a different logging
    // mechanism, this feature allows for easy changes
    public interface ILogger
    {
        void Debug(string message);
        void Info(string message);
        void Warning(string message);
        void Error(string message);
    }
}