﻿/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2020-11-16
 * Class     : CST-247 Enterprise Computer Programming III
 * Professor : Dinesh Staphit
 * Assignment: Benchmark - BibleVerse
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Create Database to store: Testament, Books, Chapters, Verses
 * 2. Create UI to allow entry and search for verses
 * 3. Use NLog for logging
 * 4. Use Unity for IoC
 * 5. Implement DAO design pattern
 * ---------------------------------------------------------------
 */

using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BibleVerse.Services.Utility
{
    public class BibleVerseLogger : ILogger
    {
        // Singleton Pattern
        private static BibleVerseLogger instance;
        private static Logger logger;

        // Singleton Pattern - Private empty constructor
        private BibleVerseLogger() { }

        public static BibleVerseLogger GetInstance()
        {
            if (instance == null)
                instance = new BibleVerseLogger();

            return instance;
        }

        private Logger GetLogger(string theLogger)
        {
            if (BibleVerseLogger.logger == null)
                BibleVerseLogger.logger = LogManager.GetLogger(theLogger);

            return BibleVerseLogger.logger;
        }

        public void Debug(string message)
        {
            GetLogger("BibleVerseLoggerRules").Debug(message);
        }

        public void Error(string message)
        {
            GetLogger("BibleVerseLoggerRules").Error(message);
        }

        public void Info(string message)
        {
            GetLogger("BibleVerseLoggerRules").Info(message);
        }

        public void Warning(string message)
        {
            GetLogger("BibleVerseLoggerRules").Warn(message);
        }
    }
}