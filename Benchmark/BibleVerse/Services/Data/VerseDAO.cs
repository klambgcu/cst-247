﻿/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2020-11-16
 * Class     : CST-247 Enterprise Computer Programming III
 * Professor : Dinesh Staphit
 * Assignment: Benchmark - BibleVerse
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Create Database to store: Testament, Books, Chapters, Verses
 * 2. Create UI to allow entry and search for verses
 * 3. Use NLog for logging
 * 4. Use Unity for IoC
 * 5. Implement DAO design pattern
 * ---------------------------------------------------------------
 */

using BibleVerse.Models;
using BibleVerse.Services.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace BibleVerse.Services.Data
{
    // Encapsulate Database Access for Verse Table
    public class VerseDAO
    {
        private ILogger logger;
        private string connectionString;

        public VerseDAO(ILogger logger, string connectionString)
        {
            this.logger = logger;
            this.connectionString = connectionString;
        }

        public Verse InsertUpdate(Verse verse)
        {
            verse = Exists(verse, false);

            if (verse.ID < 1)
            {
                verse = InsertDB(verse);
            }
            else
            {
                UpdateDB(verse);
            }
            return verse;
        }

        // Check if verse already exists
        // Returns verse with ID set to row ID or -1 (not exists)
        public Verse Exists(Verse verse, Boolean updateText)
        {
            logger.Info("Exists:Enter");

            long verseID = -1;
            string queryString = "SELECT ID, Text FROM dbo.Verse WHERE TestamentID = @TestamentID AND BookID = @BookID AND ChapterID = @ChapterID AND Number = @Number";

            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                using (SqlCommand sqlCommand = new SqlCommand(queryString, sqlConnection))
                {
                    sqlCommand.Parameters.Add("@BookID", SqlDbType.BigInt).Value = verse.BookID;
                    sqlCommand.Parameters.Add("@TestamentID", SqlDbType.BigInt).Value = verse.TestamentID;
                    sqlCommand.Parameters.Add("@ChapterID", SqlDbType.BigInt).Value = verse.ChapterID;
                    sqlCommand.Parameters.Add("@Number", SqlDbType.BigInt).Value = verse.Number;
                    try
                    {
                        sqlConnection.Open();
                        SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                        if (sqlDataReader.Read())
                        {
                            verseID = sqlDataReader.GetInt64(0);
                            if (updateText) verse.Text = sqlDataReader.GetString(1);
                        }
                        sqlConnection.Close();
                    }
                    catch (Exception e)
                    {
                        logger.Error("Exists:Database Read Failure " + e.Message);
                    }
                }
            }
            verse.ID = verseID;
            return verse;
        }

        private Verse InsertDB(Verse verse)
        {
            logger.Info("InsertDB:Enter");

            string queryString = "INSERT INTO dbo.Verse (ChapterID, ChapterNumber, BookID, TestamentID, Number, Text) VALUES ( @ChapterID, @ChapterNumber, @BookID, @TestamentID, @Number, @Text ); SELECT CAST(scope_identity() AS int)";
            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                using (SqlCommand sqlCommand = new SqlCommand(queryString, sqlConnection))
                {
                    sqlCommand.Parameters.Add("@ChapterID", SqlDbType.BigInt).Value = verse.ChapterID;
                    sqlCommand.Parameters.Add("@ChapterNumber", SqlDbType.Int).Value = verse.ChapterNumber;
                    sqlCommand.Parameters.Add("@BookID", SqlDbType.BigInt).Value = verse.BookID;
                    sqlCommand.Parameters.Add("@TestamentID", SqlDbType.BigInt).Value = verse.TestamentID;
                    sqlCommand.Parameters.Add("@Number", SqlDbType.Int).Value = verse.Number;
                    sqlCommand.Parameters.Add("@Text", SqlDbType.Text).Value = verse.Text;

                    try
                    {
                        sqlConnection.Open();
                        long verseID = Convert.ToInt64(sqlCommand.ExecuteScalar()); // execute and get new row id
                        verse.ID = verseID; // store row id in verse
                        sqlConnection.Close();
                    }
                    catch (Exception e)
                    {
                        logger.Error("InsertDB:Insert Verse Failure" + e.Message);
                    }
                }
            }
            return verse;
        }

        private void UpdateDB(Verse verse)
        {
            logger.Info("UpdateDB:Enter");

            string queryString = "UPDATE dbo.Verse SET Text = @Text WHERE ID = @VerseID";
            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                using (SqlCommand sqlCommand = new SqlCommand(queryString, sqlConnection))
                {
                    sqlCommand.Parameters.Add("@VerseID", SqlDbType.BigInt).Value = verse.ID;
                    sqlCommand.Parameters.Add("@Text", SqlDbType.Text).Value = verse.Text;

                    try
                    {
                        sqlConnection.Open();
                        sqlCommand.ExecuteScalar();
                        sqlConnection.Close();
                    }
                    catch (Exception e)
                    {
                        logger.Error("UpdateDB:Update Verse Failure" + e.Message);
                    }
                }
            }
        }
    }
}