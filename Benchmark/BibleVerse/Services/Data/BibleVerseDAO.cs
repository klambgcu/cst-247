﻿/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2020-11-16
 * Class     : CST-247 Enterprise Computer Programming III
 * Professor : Dinesh Staphit
 * Assignment: Benchmark - BibleVerse
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Create Database to store: Testament, Books, Chapters, Verses
 * 2. Create UI to allow entry and search for verses
 * 3. Use NLog for logging
 * 4. Use Unity for IoC
 * 5. Implement DAO design pattern
 * ---------------------------------------------------------------
 */

using BibleVerse.Models;
using BibleVerse.Services.Data;
using BibleVerse.Services.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace BibleVerse.Services.Data
{
    // Encapsulate Database Access for Application
    public class BibleVerseDAO : IBibleVerseDAO
    {
        private ILogger logger;
        private TestamentDAO testamentDAO;
        private BookDAO bookDAO;
        private ChapterDAO chapterDAO;
        private VerseDAO verseDAO;

        private string connectionString = @"Data Source=localhost\SQLEXPRESS;Initial Catalog=CST247_Bible;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

        // non-default constructor - receives a logger via IoC from Home Controller
        // Instantiates component DAO 
        public BibleVerseDAO(ILogger logger)
        {
            this.logger = logger;
            testamentDAO = new TestamentDAO(logger, connectionString);
            bookDAO = new BookDAO(logger, connectionString);
            chapterDAO = new ChapterDAO(logger, connectionString);
            verseDAO = new VerseDAO(logger, connectionString);
        }

        public List<Testament> GetTestamentList()
        {
            return testamentDAO.GetTestamentList();
        }

        public List<Book> GetBookList()
        {
            return bookDAO.GetBookList();
        }

        public List<Chapter> GetChapterList()
        {
            return chapterDAO.GetChapterList();
        }

        // Save or update verse
        // Returns true for success
        // Throws database exception
        public Boolean SaveUpdateVerse(Verse verse)
        {
            Boolean results = true;
            logger.Info("SaveUpdateVerse:Enter");

            try
            {
                long chapterRowID = chapterDAO.AddChapter(
                    new Chapter
                    {
                        BookID = verse.BookID,
                        TestamentID = verse.TestamentID,
                        Number = verse.ChapterNumber
                    });

                verse.ChapterID = chapterRowID;

                verseDAO.InsertUpdate(verse);
            }
            catch(Exception e)
            {
                logger.Info("SaveUpdateVerse:Database Failure");
                results = false;
                throw e;
            }
 
            return results;
        }

        // Search for existing verse
        // Return string showing results
        // If found, Book,Verse, Text returned
        // If not found, Book, Verse, Not Found returned
        public string SearchForVerse(Verse verse)
        {
            string results = "";
            logger.Info("SearchForVerse: Entering Read Section");

            verse.ChapterID = chapterDAO.Exists(
                new Chapter
                {
                    BookID = verse.BookID,
                    TestamentID = verse.TestamentID,
                    Number = verse.ChapterNumber
                });

            verse = verseDAO.Exists(verse, true);

            var books = bookDAO.GetBookList().Where(b => b.TestamentID == verse.TestamentID && b.ID == verse.BookID).ToList();
            string name = books[0].Name;

            results = name + " " + verse.ChapterNumber.ToString() + ":" + verse.Number.ToString() + " = ";

            if (verse.ID < 1)
            {
                results = results + "Not Found.";
            }
            else
            {
                results = results + verse.Text;
            }
            return results;
        }
    }
}