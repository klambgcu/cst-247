﻿/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2020-11-16
 * Class     : CST-247 Enterprise Computer Programming III
 * Professor : Dinesh Staphit
 * Assignment: Benchmark - BibleVerse
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Create Database to store: Testament, Books, Chapters, Verses
 * 2. Create UI to allow entry and search for verses
 * 3. Use NLog for logging
 * 4. Use Unity for IoC
 * 5. Implement DAO design pattern
 * ---------------------------------------------------------------
 */

using BibleVerse.Models;
using BibleVerse.Services.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace BibleVerse.Services.Data
{
    // Encapsulate Database Access for Chapter Table
    public class ChapterDAO
    {
        private ILogger logger;
        private string connectionString;
        private List<Chapter> ChapterList = new List<Chapter>();

        public ChapterDAO(ILogger logger, string connectionString)
        {
            this.logger = logger;
            this.connectionString = connectionString;
        }

        public List<Chapter> GetChapterList()
        {
            logger.Info("GetChapterList: Entry");
            if (ChapterList.Count == 0)
            {
                logger.Info("GetChapterList: ChapterList is empty - fetch from database");
                LoadAllChapters();
            }

            return ChapterList;
        }

        private void LoadAllChapters()
        {
            logger.Info("LoadAllChapters: Entering Load Section");
            String queryString = "SELECT ID, BookID, TestamentID, Number FROM dbo.Chapter ORDER BY TestamentID, BookID, Number ASC";

            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                using (SqlCommand sqlCommand = new SqlCommand(queryString, sqlConnection))
                {
                    try
                    {
                        sqlConnection.Open();
                        SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                        while (sqlDataReader.Read())
                        {
                            ChapterList.Add(
                                new Chapter
                                {
                                    ID = sqlDataReader.GetInt64(0),
                                    BookID = sqlDataReader.GetInt64(1),
                                    TestamentID = sqlDataReader.GetInt64(2),
                                    Number = sqlDataReader.GetInt32(3)
                                });
                        }
                        sqlConnection.Close();
                    }
                    catch (Exception e)
                    {
                        logger.Error("LoadAllChapters: Database Read Error Occurred! " + e.Message);
                        ChapterList.Clear();
                    }
                }
            }
        }

        // Checks if chapter already exists
        // returns rowID if found, -1 if not found
        public long Exists(Chapter chapter)
        {
            if (ChapterList.Count() == 0)
            {
                LoadAllChapters();
            }

            // Check if it already exists
            var chapters = ChapterList.Where(c => c.TestamentID == chapter.TestamentID && c.BookID == chapter.BookID && c.Number == chapter.Number).ToList();

            if (chapters.Count == 0)
                return -1;
            else
                return chapters[0].ID;
        }

        // Add record if needed, returns row ID for record
        public long AddChapter(Chapter chapter)
        {
            long chapterID = Exists(chapter);

            if (chapterID == -1) // Does not exist, create it
            {
                // Insert into database, add to ChapterList, return new rowID
                chapter = InsertDB(chapter);
                ChapterList.Add(chapter);
                chapterID = chapter.ID;
            }

            return chapterID;
        }

        // Create a new record in chapter, return chapter with row ID
        private Chapter InsertDB(Chapter chapter)
        {
            logger.Info("InsertDB:Chapter Create");

            string queryString = "INSERT INTO dbo.Chapter (BookID, TestamentID, Number) VALUES ( @BookID, @TestamentID, @Number ); SELECT CAST(scope_identity() AS int)";
            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                using (SqlCommand sqlCommand = new SqlCommand(queryString, sqlConnection))
                {
                    sqlCommand.Parameters.Add("@BookID", SqlDbType.BigInt).Value = chapter.BookID;
                    sqlCommand.Parameters.Add("@TestamentID", SqlDbType.BigInt).Value = chapter.TestamentID;
                    sqlCommand.Parameters.Add("@Number", SqlDbType.Int).Value = chapter.Number;

                    try
                    {
                        sqlConnection.Open();
                        long chapterID = Convert.ToInt64(sqlCommand.ExecuteScalar()); // execute and get new row id
                        chapter.ID = chapterID; // store row id in verse
                        sqlConnection.Close();
                    }
                    catch (Exception e)
                    {
                        logger.Error("InsertDB:Chapter Create " + e.Message);
                    }
                }
            }
            return chapter;
        }
    }
}