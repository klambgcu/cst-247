﻿/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2020-11-16
 * Class     : CST-247 Enterprise Computer Programming III
 * Professor : Dinesh Staphit
 * Assignment: Benchmark - BibleVerse
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Create Database to store: Testament, Books, Chapters, Verses
 * 2. Create UI to allow entry and search for verses
 * 3. Use NLog for logging
 * 4. Use Unity for IoC
 * 5. Implement DAO design pattern
 * ---------------------------------------------------------------
 */

using BibleVerse.Models;
using BibleVerse.Services.Utility;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace BibleVerse.Services.Data
{
    // Encapsulate Database Access for Book Table
    public class BookDAO
    {
        private ILogger logger;
        private string connectionString;
        private List<Book> BookList = new List<Book>();

        public BookDAO(ILogger logger, string connectionString)
        {
            this.logger = logger;
            this.connectionString = connectionString;
        }

        public List<Book> GetBookList()
        {
            logger.Info("GetBookList: Entry");
            if (BookList.Count == 0)
            {
                logger.Info("GetBookList: BookList is empty - fetch from database");
                LoadAllBooks();
            }

            return BookList;
        }

        private void LoadAllBooks()
        {
            logger.Info("LoadAllBooks: Entering Load Section");
            String queryString = "SELECT ID, TestamentID, Name FROM dbo.Book ORDER BY TestamentID, ID ASC";

            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                using (SqlCommand sqlCommand = new SqlCommand(queryString, sqlConnection))
                {
                    try
                    {
                        sqlConnection.Open();
                        SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                        while (sqlDataReader.Read())
                        {
                            BookList.Add(
                                new Book
                                {
                                    ID = sqlDataReader.GetInt64(0),
                                    TestamentID = sqlDataReader.GetInt64(1),
                                    Name = sqlDataReader.GetString(2)
                                });
                        }
                        sqlConnection.Close();
                    }
                    catch (Exception e)
                    {
                        logger.Error("LoadAllBooks: Database Read Error Occurred! " + e.Message);
                        BookList.Clear();
                    }
                }
            }
        }
    }
}