﻿/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2020-11-16
 * Class     : CST-247 Enterprise Computer Programming III
 * Professor : Dinesh Staphit
 * Assignment: Benchmark - BibleVerse
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Create Database to store: Testament, Books, Chapters, Verses
 * 2. Create UI to allow entry and search for verses
 * 3. Use NLog for logging
 * 4. Use Unity for IoC
 * 5. Implement DAO design pattern
 * ---------------------------------------------------------------
 */

using BibleVerse.Models;
using BibleVerse.Services.Utility;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace BibleVerse.Services.Data
{
    // Encapsulate Database Access for Testament Table
    public class TestamentDAO
    {
        private ILogger logger;
        private string connectionString;
        private List<Testament> TestamentList = new List<Testament>();

        public TestamentDAO(ILogger logger, string connectionString)
        {
            this.logger = logger;
            this.connectionString = connectionString;
        }

        public List<Testament> GetTestamentList()
        {
            logger.Info("GetTestamentList: Entry");
            if (TestamentList.Count == 0)
            {
                logger.Info("GetTestamentList: TestamentList is empty - fetch from database");
                LoadAllTestaments();
            }

            return TestamentList;
        }

        private void LoadAllTestaments()
        {
            logger.Info("LoadAllTestaments: Entering Load Section");
            String queryString = "SELECT ID, Name FROM dbo.Testament ORDER BY ID ASC";

            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                using (SqlCommand sqlCommand = new SqlCommand(queryString, sqlConnection))
                {
                    try
                    {
                        sqlConnection.Open();
                        SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                        while (sqlDataReader.Read())
                        {
                            TestamentList.Add(
                                new Testament
                                {
                                    ID = sqlDataReader.GetInt64(0),
                                    Name = sqlDataReader.GetString(1)
                                });

                        }
                        sqlConnection.Close();
                    }
                    catch (Exception e)
                    {
                        logger.Error("LoadAllTestaments: Database Read Error Occurred! " + e.Message);
                        TestamentList.Clear();
                    }
                }
            }
        }

    }
}