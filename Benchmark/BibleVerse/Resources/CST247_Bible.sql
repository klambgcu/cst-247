USE [master]
GO

/****** Object:  Database [CST247_Bible]    Script Date: 11/20/2020 3:49:05 PM ******/
CREATE DATABASE [CST247_Bible]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'CST247_Bible', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\CST247_Bible.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'CST247_Bible_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\CST247_Bible.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO

IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [CST247_Bible].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO

ALTER DATABASE [CST247_Bible] SET ANSI_NULL_DEFAULT ON 
GO

ALTER DATABASE [CST247_Bible] SET ANSI_NULLS ON 
GO

ALTER DATABASE [CST247_Bible] SET ANSI_PADDING ON 
GO

ALTER DATABASE [CST247_Bible] SET ANSI_WARNINGS ON 
GO

ALTER DATABASE [CST247_Bible] SET ARITHABORT ON 
GO

ALTER DATABASE [CST247_Bible] SET AUTO_CLOSE OFF 
GO

ALTER DATABASE [CST247_Bible] SET AUTO_SHRINK OFF 
GO

ALTER DATABASE [CST247_Bible] SET AUTO_UPDATE_STATISTICS ON 
GO

ALTER DATABASE [CST247_Bible] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO

ALTER DATABASE [CST247_Bible] SET CURSOR_DEFAULT  LOCAL 
GO

ALTER DATABASE [CST247_Bible] SET CONCAT_NULL_YIELDS_NULL ON 
GO

ALTER DATABASE [CST247_Bible] SET NUMERIC_ROUNDABORT OFF 
GO

ALTER DATABASE [CST247_Bible] SET QUOTED_IDENTIFIER ON 
GO

ALTER DATABASE [CST247_Bible] SET RECURSIVE_TRIGGERS OFF 
GO

ALTER DATABASE [CST247_Bible] SET  DISABLE_BROKER 
GO

ALTER DATABASE [CST247_Bible] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO

ALTER DATABASE [CST247_Bible] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO

ALTER DATABASE [CST247_Bible] SET TRUSTWORTHY OFF 
GO

ALTER DATABASE [CST247_Bible] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO

ALTER DATABASE [CST247_Bible] SET PARAMETERIZATION SIMPLE 
GO

ALTER DATABASE [CST247_Bible] SET READ_COMMITTED_SNAPSHOT OFF 
GO

ALTER DATABASE [CST247_Bible] SET HONOR_BROKER_PRIORITY OFF 
GO

ALTER DATABASE [CST247_Bible] SET RECOVERY FULL 
GO

ALTER DATABASE [CST247_Bible] SET  MULTI_USER 
GO

ALTER DATABASE [CST247_Bible] SET PAGE_VERIFY CHECKSUM  
GO

ALTER DATABASE [CST247_Bible] SET DB_CHAINING OFF 
GO

ALTER DATABASE [CST247_Bible] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO

ALTER DATABASE [CST247_Bible] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO

ALTER DATABASE [CST247_Bible] SET DELAYED_DURABILITY = DISABLED 
GO

ALTER DATABASE [CST247_Bible] SET QUERY_STORE = OFF
GO

ALTER DATABASE [CST247_Bible] SET  READ_WRITE 
GO


USE [CST247_Bible]
GO

/****** Object:  Table [dbo].[Testament]    Script Date: 11/20/2020 3:50:14 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Testament](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

USE [CST247_Bible]
GO

/****** Object:  Table [dbo].[Book]    Script Date: 11/20/2020 3:50:48 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Book](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[TestamentID] [bigint] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Book]  WITH CHECK ADD  CONSTRAINT [FK_Book_Testament] FOREIGN KEY([TestamentID])
REFERENCES [dbo].[Testament] ([ID])
GO

ALTER TABLE [dbo].[Book] CHECK CONSTRAINT [FK_Book_Testament]
GO

USE [CST247_Bible]
GO

/****** Object:  Table [dbo].[Chapter]    Script Date: 11/20/2020 3:51:03 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Chapter](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[BookID] [bigint] NOT NULL,
	[TestamentID] [bigint] NOT NULL,
	[Number] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Chapter]  WITH CHECK ADD  CONSTRAINT [FK_Chapter_Book] FOREIGN KEY([BookID])
REFERENCES [dbo].[Book] ([ID])
GO

ALTER TABLE [dbo].[Chapter] CHECK CONSTRAINT [FK_Chapter_Book]
GO

ALTER TABLE [dbo].[Chapter]  WITH CHECK ADD  CONSTRAINT [FK_Chapter_Testament] FOREIGN KEY([TestamentID])
REFERENCES [dbo].[Testament] ([ID])
GO

ALTER TABLE [dbo].[Chapter] CHECK CONSTRAINT [FK_Chapter_Testament]
GO

USE [CST247_Bible]
GO

/****** Object:  Table [dbo].[Verse]    Script Date: 11/20/2020 3:51:21 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

USE [CST247_Bible]
GO

/****** Object:  Table [dbo].[Verse]    Script Date: 11/20/2020 11:32:47 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Verse](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ChapterID] [bigint] NOT NULL,
	[ChapterNumber] [int] NOT NULL,
	[BookID] [bigint] NOT NULL,
	[TestamentID] [bigint] NOT NULL,
	[Number] [int] NOT NULL,
	[Text] [nvarchar](600) NOT NULL,
 CONSTRAINT [PK__Verse__3214EC277BD686D4] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Verse]  WITH CHECK ADD  CONSTRAINT [FK_Verse_Book1] FOREIGN KEY([BookID])
REFERENCES [dbo].[Book] ([ID])
GO

ALTER TABLE [dbo].[Verse] CHECK CONSTRAINT [FK_Verse_Book1]
GO

ALTER TABLE [dbo].[Verse]  WITH CHECK ADD  CONSTRAINT [FK_Verse_Chapter1] FOREIGN KEY([ChapterID])
REFERENCES [dbo].[Chapter] ([ID])
GO

ALTER TABLE [dbo].[Verse] CHECK CONSTRAINT [FK_Verse_Chapter1]
GO

ALTER TABLE [dbo].[Verse]  WITH CHECK ADD  CONSTRAINT [FK_Verse_Testament] FOREIGN KEY([TestamentID])
REFERENCES [dbo].[Testament] ([ID])
GO

ALTER TABLE [dbo].[Verse] CHECK CONSTRAINT [FK_Verse_Testament]
GO



/******************************************************
 * Populate data for Old Testament and Books
 ******************************************************/
USE [CST247_Bible]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

INSERT INTO [dbo].[Testament] ([Name]) VALUES ('Old Testament')
INSERT INTO [dbo].[Testament] ([Name]) VALUES ('New Testament')
GO

/* Old Testament Books */
INSERT INTO [dbo].[Book] ([TestamentID],[Name]) VALUES (1,'Genesis')
INSERT INTO [dbo].[Book] ([TestamentID],[Name]) VALUES (1,'Exodus')
INSERT INTO [dbo].[Book] ([TestamentID],[Name]) VALUES (1,'Leviticus')
INSERT INTO [dbo].[Book] ([TestamentID],[Name]) VALUES (1,'Numbers')
INSERT INTO [dbo].[Book] ([TestamentID],[Name]) VALUES (1,'Deuteronomy')
INSERT INTO [dbo].[Book] ([TestamentID],[Name]) VALUES (1,'Joshua')
INSERT INTO [dbo].[Book] ([TestamentID],[Name]) VALUES (1,'Judges')
INSERT INTO [dbo].[Book] ([TestamentID],[Name]) VALUES (1,'Ruth')
INSERT INTO [dbo].[Book] ([TestamentID],[Name]) VALUES (1,'1 Samuel')
INSERT INTO [dbo].[Book] ([TestamentID],[Name]) VALUES (1,'2 Samuel')
INSERT INTO [dbo].[Book] ([TestamentID],[Name]) VALUES (1,'1 Kings')
INSERT INTO [dbo].[Book] ([TestamentID],[Name]) VALUES (1,'2 Kings')
INSERT INTO [dbo].[Book] ([TestamentID],[Name]) VALUES (1,'1 Chronicles')
INSERT INTO [dbo].[Book] ([TestamentID],[Name]) VALUES (1,'2 Chronicles')
INSERT INTO [dbo].[Book] ([TestamentID],[Name]) VALUES (1,'Ezra')
INSERT INTO [dbo].[Book] ([TestamentID],[Name]) VALUES (1,'Nehemiah')
INSERT INTO [dbo].[Book] ([TestamentID],[Name]) VALUES (1,'Esther')
INSERT INTO [dbo].[Book] ([TestamentID],[Name]) VALUES (1,'Job')
INSERT INTO [dbo].[Book] ([TestamentID],[Name]) VALUES (1,'Psalms')
INSERT INTO [dbo].[Book] ([TestamentID],[Name]) VALUES (1,'Proverbs')
INSERT INTO [dbo].[Book] ([TestamentID],[Name]) VALUES (1,'Ecclesiastes')
INSERT INTO [dbo].[Book] ([TestamentID],[Name]) VALUES (1,'Song of Solomon')
INSERT INTO [dbo].[Book] ([TestamentID],[Name]) VALUES (1,'Isaiah')
INSERT INTO [dbo].[Book] ([TestamentID],[Name]) VALUES (1,'Jeremiah')
INSERT INTO [dbo].[Book] ([TestamentID],[Name]) VALUES (1,'Lamentations')
INSERT INTO [dbo].[Book] ([TestamentID],[Name]) VALUES (1,'Ezekiel')
INSERT INTO [dbo].[Book] ([TestamentID],[Name]) VALUES (1,'Daniel')
INSERT INTO [dbo].[Book] ([TestamentID],[Name]) VALUES (1,'Hosea')
INSERT INTO [dbo].[Book] ([TestamentID],[Name]) VALUES (1,'Joel')
INSERT INTO [dbo].[Book] ([TestamentID],[Name]) VALUES (1,'Amos')
INSERT INTO [dbo].[Book] ([TestamentID],[Name]) VALUES (1,'Obadiah')
INSERT INTO [dbo].[Book] ([TestamentID],[Name]) VALUES (1,'Jonah')
INSERT INTO [dbo].[Book] ([TestamentID],[Name]) VALUES (1,'Micah')
INSERT INTO [dbo].[Book] ([TestamentID],[Name]) VALUES (1,'Nahum')
INSERT INTO [dbo].[Book] ([TestamentID],[Name]) VALUES (1,'Habakkuk')
INSERT INTO [dbo].[Book] ([TestamentID],[Name]) VALUES (1,'Zephaniah')
INSERT INTO [dbo].[Book] ([TestamentID],[Name]) VALUES (1,'Haggai')
INSERT INTO [dbo].[Book] ([TestamentID],[Name]) VALUES (1,'Zechariah')
INSERT INTO [dbo].[Book] ([TestamentID],[Name]) VALUES (1,'Malachi')

/* New Testament Books */
INSERT INTO [dbo].[Book] ([TestamentID],[Name]) VALUES (2,'Matthew')
INSERT INTO [dbo].[Book] ([TestamentID],[Name]) VALUES (2,'Mark')
INSERT INTO [dbo].[Book] ([TestamentID],[Name]) VALUES (2,'Luke')
INSERT INTO [dbo].[Book] ([TestamentID],[Name]) VALUES (2,'John')
INSERT INTO [dbo].[Book] ([TestamentID],[Name]) VALUES (2,'Acts (of the Apostles)')
INSERT INTO [dbo].[Book] ([TestamentID],[Name]) VALUES (2,'Romans')
INSERT INTO [dbo].[Book] ([TestamentID],[Name]) VALUES (2,'1 Corinthians')
INSERT INTO [dbo].[Book] ([TestamentID],[Name]) VALUES (2,'2 Corinthians')
INSERT INTO [dbo].[Book] ([TestamentID],[Name]) VALUES (2,'Galatians')
INSERT INTO [dbo].[Book] ([TestamentID],[Name]) VALUES (2,'Ephesians')
INSERT INTO [dbo].[Book] ([TestamentID],[Name]) VALUES (2,'Philippians')
INSERT INTO [dbo].[Book] ([TestamentID],[Name]) VALUES (2,'Colossians')
INSERT INTO [dbo].[Book] ([TestamentID],[Name]) VALUES (2,'1 Thessalonians')
INSERT INTO [dbo].[Book] ([TestamentID],[Name]) VALUES (2,'2 Thessalonians')
INSERT INTO [dbo].[Book] ([TestamentID],[Name]) VALUES (2,'1 Timothy')
INSERT INTO [dbo].[Book] ([TestamentID],[Name]) VALUES (2,'2 Timothy')
INSERT INTO [dbo].[Book] ([TestamentID],[Name]) VALUES (2,'Titus')
INSERT INTO [dbo].[Book] ([TestamentID],[Name]) VALUES (2,'Philemon')
INSERT INTO [dbo].[Book] ([TestamentID],[Name]) VALUES (2,'Hebrews')
INSERT INTO [dbo].[Book] ([TestamentID],[Name]) VALUES (2,'James')
INSERT INTO [dbo].[Book] ([TestamentID],[Name]) VALUES (2,'1 Peter')
INSERT INTO [dbo].[Book] ([TestamentID],[Name]) VALUES (2,'2 Peter')
INSERT INTO [dbo].[Book] ([TestamentID],[Name]) VALUES (2,'1 John')
INSERT INTO [dbo].[Book] ([TestamentID],[Name]) VALUES (2,'2 John')
INSERT INTO [dbo].[Book] ([TestamentID],[Name]) VALUES (2,'3 John')
INSERT INTO [dbo].[Book] ([TestamentID],[Name]) VALUES (2,'Jude')
INSERT INTO [dbo].[Book] ([TestamentID],[Name]) VALUES (2,'Revelation')
GO

