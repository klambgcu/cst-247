﻿/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2020-11-16
 * Class     : CST-247 Enterprise Computer Programming III
 * Professor : Dinesh Staphit
 * Assignment: Benchmark - BibleVerse
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Create Database to store: Testament, Books, Chapters, Verses
 * 2. Create UI to allow entry and search for verses
 * 3. Use NLog for logging
 * 4. Use Unity for IoC
 * 5. Implement DAO design pattern
 * ---------------------------------------------------------------
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BibleVerse.Models;
using BibleVerse.Services.Data;
using BibleVerse.Services.Utility;

namespace BibleVerse.Controllers
{
    public class HomeController : Controller
    {
        private ILogger logger;
        private IBibleVerseDAO bibleVerseDAO;

        // non-default constructor - receives a logger via IoC
        public HomeController(ILogger logger)
        {
            this.logger = logger;
            bibleVerseDAO = new BibleVerseDAO(logger);
        }

        /* Displays the Welcome / Home page */
        public ActionResult Index()
        {
            logger.Info("Home:Index-We have a visitor.");
            return View();
        }

        /* Displays an information page */
        public ActionResult About()
        {
            logger.Info("Home:About-Someone is curious about the application.");
            ViewBag.Message = "Your about page.";
            return View();
        }

        /* Displays the verse creation entry page */
        public ActionResult Create()
        {
            logger.Info("Home:Create-Starting verse entry form.");
            
            return View("Create", new Verse());
        }

        // Receives verse creation information in verseModel
        // Saves/updates information to the database
        // Returns to the entry screen to allow another verse entry
        // Accepts only post entries
        [HttpPost]
        public ActionResult Create(Verse verseModel)
        {
            logger.Info("Home:Create-A verse received-send to database");
            Verse temp = verseModel;

            Boolean results = bibleVerseDAO.SaveUpdateVerse(verseModel);

            ModelState.Clear();
            return View("Create", new Verse());
        }

        // Receives testID (1=Old Testament, 2=New Testament)
        // Builds and returns a json object for the Book Selection Dropdown list
        // Form utilizes Ajax to populate book selection based upon selected testament
        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult LoadBookList(string testID)
        {
            logger.Info("Home:LoadBookList - testament id: " + testID);
            var bookList = GetAllBookList(long.Parse(testID));
            return Json(bookList, JsonRequestBehavior.AllowGet);
        }

        // Helper function for LoadBookList - queries the database for the
        // list of books for the selected testament and returns a list of
        // SelectListItems for the Book Selection Dropdown list
        [NonAction]
        public IEnumerable<SelectListItem> GetAllBookList(long selectedTestID)
        {
            //generate empty list
            var selectList = new List<SelectListItem>();
            
            // Get Booklist for DB or cached List
            List<Book> bookList = bibleVerseDAO.GetBookList();

            // Filter book list to specific testament
            var books = bookList.Where(b => b.TestamentID == selectedTestID);

            // populate selection list
            foreach (var book in books)
            {
                //add elements in dropdown
                selectList.Add(new SelectListItem
                {
                    Value = book.ID.ToString(),
                    Text = book.Name
                });
            }
            logger.Info("Home:GetAllBookList return");
            return selectList;
        }

        // Displays the initial verse search page query form
        public ActionResult Search()
        {
            logger.Info("Home:Search-Initial search query form.");
            return View();
        }

        // Retrieves the verseModel from the search query form
        // Searches the database and returns the results
        // Accepts only post entries
        [HttpPost]
        public ActionResult Search(Verse verseModel)
        {
            logger.Info("Home:Search-Received search query database, return results.");
            String searchResults = bibleVerseDAO.SearchForVerse(verseModel);

            ViewBag.Message = searchResults;
            return View("SearchResults");
        }
    }
}