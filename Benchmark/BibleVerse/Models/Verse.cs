﻿/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2020-11-16
 * Class     : CST-247 Enterprise Computer Programming III
 * Professor : Dinesh Staphit
 * Assignment: Benchmark - BibleVerse
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Create Database to store: Testament, Books, Chapters, Verses
 * 2. Create UI to allow entry and search for verses
 * 3. Use NLog for logging
 * 4. Use Unity for IoC
 * 5. Implement DAO design pattern
 * ---------------------------------------------------------------
 */

using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace BibleVerse.Models
{
    // Class to contain a bible verse along with id values
    // for database relationships
    public class Verse
    {
        [Required]
        public long ID { get; set; }
 
        public long ChapterID { get; set; }

        [Required]
        [DisplayName("Select Book")]
        [Range(1, 66, ErrorMessage = "Choose a valid book.")]
        public long BookID { get; set; }


        [Required]
        [DisplayName("Chapter Number")]
        [Range(1, 1000, ErrorMessage = "Enter valid chapter.")]
        public int ChapterNumber { get; set; }


        [Required]
        [DisplayName("Select Testament")]
        [Range(1, 2, ErrorMessage = "Choose a valid testament.")]
        public long TestamentID { get; set; }

 
        [Required]
        [DisplayName("Verse Number")]
        [Range(1, 1000, ErrorMessage = "Enter a valid verse.")]
        public int Number { get; set; }


        [Required]
        [DisplayName("Verse Text")]
        public string Text { get; set; }
    }
}