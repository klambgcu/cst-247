﻿using Activity2Part3.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace HelloWorldService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class UserService : IUserService
    {
        private List<UserModel> users;

        public UserService()
        {
            // Create list of new users
            users = new List<UserModel>();
            users.Add(new UserModel { Username = "Mickey", Password = "Mouse" });
            users.Add(new UserModel { Username = "Minnie", Password = "Mouse" });
            users.Add(new UserModel { Username = "Donald", Password = "Duck" });
            users.Add(new UserModel { Username = "Daisy", Password = "Duck" });
            users.Add(new UserModel { Username = "Clarabelle", Password = "Cow" });
            users.Add(new UserModel { Username = "Oswald", Password = "Rabbit" });
        }

        public DTO GetAllUsers()
        {
            DTO response = new DTO();

            response.ErrorCode = 0;
            response.ErrorMsg = "OK";
            response.Data = users;

            return response;
        }

        public string GetData(string value)
        {
            int v = Int32.Parse(value);
            int v2 = v * v;
            int v3 = v * v * v;
            return "You entered value: " + value + ". Squared (" + v2.ToString() + ") Cubed (" + v3.ToString() + ").";
        }

        public CompositeType GetObjectModel(string id)
        {
            CompositeType test = new CompositeType();
            test.Value = Int32.Parse(id);

            if (test.Value >= 0)
            {
                test.HappyHello = true;
                test.HelloMessage = "Hey, I hope you are having a great day!";
            }
            else
            {
                test.HelloMessage = "Well, I think we need to look on the positive side of life.";
            }

            return test;
        }

        public DTO GetUser(string id)
        {
            DTO response = new DTO();

            try
            {
                int pos = Int32.Parse(id);
                if (pos >= 0 && pos < users.Count)
                {
                    response.ErrorCode = 0;
                    response.ErrorMsg = "OK";
                    List<UserModel> list = new List<UserModel>();
                    list.Add(users[pos]);
                    response.Data = list;
                }
                else
                {
                    response.ErrorCode = -1;
                    response.ErrorMsg = "User Does Not Exist";
                    response.Data = null;
                }
            }
            catch (Exception e)
            {
                response.ErrorCode = -1;
                response.ErrorMsg = "Invalid argument";
                response.Data = null;
            }

            return response;
        }

        public string SayHello()
        {
            return "Hi my friend! I am Kelly Lamb. John 1:29 John the Baptist sees Jesus and exclaims, Behold the Lamb of God who takes away the sin of the world.";
        }
    }
}
