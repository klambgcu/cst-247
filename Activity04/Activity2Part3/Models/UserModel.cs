/*
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2020-10-05
 * Class     : CST-247 Enterprise Computer Programming III
 * Professor : Dinesh Sthapit M.S.
 * Assignment: Activity 1-3 (Data Validation Activity 2-3)
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * Login authentication: Create SQL table and validate user entry
 * ---------------------------------------------------------------
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Activity2Part3.Models
{
    [DataContract]
    public class UserModel
    {
        [Required]
        [DisplayName("User Name")]
        [StringLength(20, MinimumLength = 4)]
        [DefaultValue("")]
        [DataMember]
        public string Username { get; set; }

        [Required]
        [DisplayName("Password")]
        [StringLength(20, MinimumLength = 4)]
        [DefaultValue("")]
        [DataMember]
        public string Password { get; set; }
    }
}