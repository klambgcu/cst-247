/*
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2020-10-05
 * Class     : CST-247 Enterprise Computer Programming III
 * Professor : Dinesh Sthapit M.S.
 * Assignment: Activity 1-3
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * Login authentication: Create SQL table and validate user entry
 * ---------------------------------------------------------------
 */

using Activity2Part3.Models;
using Activity2Part3.Services.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Activity2Part3.Services.Business
{
    public class SecurityService
    {
        public bool Authenticate(UserModel user)
        {
            SecurityDAO service = new SecurityDAO();
            return service.FindByUser(user);
        }
    }
}