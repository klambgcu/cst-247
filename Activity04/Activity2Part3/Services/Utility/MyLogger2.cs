﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Activity2Part3.Services.Utility
{
    public class MyLogger2 : ILogger2
    {
        // Singleton Pattern
        private static MyLogger2 instance;
        private static Logger logger;

        // Singleton Pattern - Private empty constructor
        private MyLogger2 () { }

        public static MyLogger2 GetInstance()
        {
            if (instance == null)
                instance = new MyLogger2();

            return instance;
        }

        private Logger GetLogger(string theLogger)
        {
            if (MyLogger2.logger == null)
                MyLogger2.logger = LogManager.GetLogger(theLogger);

            return MyLogger2.logger;
        }

        public void Debug(string message, string arg = null)
        {
            if (arg == null)
                GetLogger("myAppLoggerRules2").Debug(message);
            else
                GetLogger("myAppLoggerRules2").Debug(message, arg);
        }

        public void Error(string message, string arg = null)
        {
            if (arg == null)
                GetLogger("myAppLoggerRules2").Error(message);
            else
                GetLogger("myAppLoggerRules2").Error(message, arg);
        }

        public void Info(string message, string arg = null)
        {
            if (arg == null)
                GetLogger("myAppLoggerRules2").Info(message);
            else
                GetLogger("myAppLoggerRules2").Info(message, arg);
        }

        public void Warning(string message, string arg = null)
        {
            if (arg == null)
                GetLogger("myAppLoggerRules2").Warn(message);
            else
                GetLogger("myAppLoggerRules2").Warn(message, arg);
        }
    }
}