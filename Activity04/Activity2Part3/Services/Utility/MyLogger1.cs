﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Activity2Part3.Services.Utility
{
    public class MyLogger1 : ILogger
    {
        // Singleton Pattern
        private static MyLogger1 instance;
        private static Logger logger;

        // Singleton Pattern - Private empty constructor
        private MyLogger1 () { }

        public static MyLogger1 GetInstance()
        {
            if (instance == null)
                instance = new MyLogger1();

            return instance;
        }

        private Logger GetLogger(string theLogger)
        {
            if (MyLogger1.logger == null)
                MyLogger1.logger = LogManager.GetLogger(theLogger);

            return MyLogger1.logger;
        }

        public void Debug(string message)
        {
            GetLogger("myAppLoggerRules").Debug(message);
        }

        public void Error(string message)
        {
            GetLogger("myAppLoggerRules").Error(message);
        }

        public void Info(string message)
        {
            GetLogger("myAppLoggerRules").Info(message);
        }

        public void Warning(string message)
        {
            GetLogger("myAppLoggerRules").Warn(message);
        }
    }
}