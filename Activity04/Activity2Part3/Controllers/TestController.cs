﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Activity2Part3.Controllers
{
    public class TestController : Controller
    {
        // GET: Test
        [HttpGet]
        [CustomAuthorization]
        public ActionResult Index()
        {
            return Content("This is a protected area in Test Controller");
        }
    }
}