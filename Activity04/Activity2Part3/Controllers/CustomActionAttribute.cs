﻿using Activity2Part3.Services.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Activity2Part3.Controllers
{
    public class CustomActionAttribute : FilterAttribute, IActionFilter
    {
        public void OnActionExecuted(ActionExecutedContext filterContext)
        {
            MyLogger1.GetInstance().Info("*** ACTION FILTER - OnActionExecuted *** " + filterContext.ToString());
        }

        public void OnActionExecuting(ActionExecutingContext filterContext)
        {
            MyLogger1.GetInstance().Info("*** ACTION FILTER - OnActionExecuting *** " + filterContext.ToString());
        }
    }
}