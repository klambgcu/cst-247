/*
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2020-10-05
 * Class     : CST-247 Enterprise Computer Programming III
 * Professor : Dinesh Sthapit M.S.
 * Assignment: Activity 1-3
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * Login authentication: Create SQL table and validate user entry
 * ---------------------------------------------------------------
 */

using Activity2Part3.Models;
using Activity2Part3.Services.Business;
using Activity2Part3.Services.Utility;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace Activity2Part3.Controllers
{
    public class LoginController : Controller
    {
        private static MyLogger1 logger = MyLogger1.GetInstance();

        // GET: Login
        [HttpGet]
        public ActionResult Index()
        {
            return View("Login");
        }

        [HttpPost]
        [CustomAction]
        public ActionResult Login(UserModel model)
        {
            // Add a log message at entry
            logger.Info("Entering LoginController.DoLogin()");

            try
            {
                // Validate the Form POST
                if (!ModelState.IsValid)
                    return View("Login");

                SecurityService service = new SecurityService();
                bool results = service.Authenticate(model);

                logger.Info("Parameters are: " + new JavaScriptSerializer().Serialize(model));

                if (results)
                {
                    logger.Info("Exit LoginControlle.DoLogin() with login passing");
                    Session["user"] = model;
                    return View("LoginPassed", model);
                }
                else
                {
                    logger.Info("Exit LoginController.DoLogin() with login failing");
                    Session.Clear();
                    return View("LoginFailed", model);
                }
            }
            catch (Exception e)
            {
                logger.Error("Exception LoginController.DoLogin() " + e.Message);
                return Content("Exception at login" + e.Message);
            }
        }

        [HttpGet]
        [CustomAuthorization]
        public ActionResult Protected()
        {
            return Content("This is a protected area. ");
        }

        public JsonResult GetUsers()
        {
            // Get reference to memory cache
            var cache = MemoryCache.Default;

            // Create list of new users
            List<UserModel> users = new List<UserModel>();

            // Get users from cache if populated
            users = (List<UserModel>)cache.Get("Users");

            // Not in cache, create some users
            if (users == null)
            {
                users = new List<UserModel>();
                users.Add(new UserModel { Username = "Mickey", Password = "Mouse" });
                users.Add(new UserModel { Username = "Minnie", Password = "Mouse" });
                users.Add(new UserModel { Username = "Donald", Password = "Duck" });
                users.Add(new UserModel { Username = "Daisy", Password = "Duck" });
                users.Add(new UserModel { Username = "Clarabelle", Password = "Cow" });
                users.Add(new UserModel { Username = "Oswald", Password = "Rabbit" });
                MyLogger1.GetInstance().Info("Users not found in memory cache. Creating a new list");

                // Place user list in cache - 60 second refresh
                var policy = new CacheItemPolicy().AbsoluteExpiration = DateTime.Now.AddMinutes(1);
                cache.Set("Users", users, policy);
            }
            else
            {
                MyLogger1.GetInstance().Info("Users found in memory cache.");
            }

            //return Content(new JavaScriptSerializer().Serialize(users));
            return Json(users, JsonRequestBehavior.AllowGet);
        }
    }
}