﻿using Activity2Part3.Models;
using Activity2Part3.Services.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Activity2Part3.Controllers
{
    public class CustomAuthorizationAttribute : FilterAttribute, IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationContext filterContext)
        {
            SecurityService securityService = new SecurityService();

            UserModel user = (UserModel) filterContext.HttpContext.Session["user"];

            bool success = false;
            
            if (user != null)
            {
                success = securityService.Authenticate(user);
            }

            if (success)
            {
                // do nothing - let controller continue 
            }
            else
            {
                filterContext.Result = new RedirectResult("/Login");
            }           
        }
    }
}