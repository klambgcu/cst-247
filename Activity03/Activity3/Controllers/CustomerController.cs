/*
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2020-11-02
 * Class     : CST-247 Enterprise Computer Programming III
 * Professor : Dinesh Sthapit M.S.
 * Assignment: Activity 3 Multiple Sections
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * Part 1: Razor Forms, Partial Views (with and without Ajax)
 * Part 2: Utilize Visual Studio Debug features
 * ---------------------------------------------------------------
 */

using Activity3.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Activity3.Controllers
{
    public class CustomerController : Controller
    {
        public CustomerModel customer;
        public List<CustomerModel> customers = new List<CustomerModel>();

        public CustomerController()
        {
            // Create list of new customers
            customers.Add(new CustomerModel(0, "Mickey Mouse", 55));
            customers.Add(new CustomerModel(1, "Minnie Mouse", 54));
            customers.Add(new CustomerModel(2, "Donald Duck", 53));
            customers.Add(new CustomerModel(3, "Daisy Duck", 52));
            customers.Add(new CustomerModel(4, "Clarabelle Cow", 26));
            customers.Add(new CustomerModel(5, "Oswald Rabbit", 58));
        }

        // GET: Customer
        public ActionResult Index()
        {
            Tuple<List<CustomerModel>, CustomerModel> tuple;
            tuple = new Tuple<List<CustomerModel>, CustomerModel>(customers, customers[0]);

            ViewBag.Title = "Ajax";
            return View("CustomerAjax", tuple);
        }

        [HttpPost]
        public ActionResult OnSelectCustomer(string Customer)
        {
            Tuple<List<CustomerModel>, CustomerModel> tuple;
            tuple = new Tuple<List<CustomerModel>, CustomerModel>(customers, customers[Int32.Parse(Customer)]);

            ViewBag.Title = "Ajax Partial";
            return PartialView("_CustomerDetails", customers[Int32.Parse(Customer)]);
        }

        [HttpPost]
        public ActionResult OnSelectCustomer1(string Customer)
        {
            Tuple<List<CustomerModel>, CustomerModel> tuple;
            tuple = new Tuple<List<CustomerModel>, CustomerModel>(customers, customers[Int32.Parse(Customer)]);

            ViewBag.Title = "No Ajax";
            return View("CustomerNoAjax", tuple);
        }

        [HttpPost]
        public string GetMoreInfo(string Customer)
        {
            return customers[Int32.Parse(Customer)].Name + " is a very well-known animated character by Walt Disney Studios";
        }
    }
}