/*
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2020-11-02
 * Class     : CST-247 Enterprise Computer Programming III
 * Professor : Dinesh Sthapit M.S.
 * Assignment: Activity 3 Multiple Sections
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * Part 1: Razor Forms, Partial Views (with and without Ajax)
 * Part 2: Utilize Visual Studio Debug features
 * ---------------------------------------------------------------
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Activity3.Models
{
    public class CustomerModel
    {
        public CustomerModel(int id, string name, int age)
        {
            ID = id;
            Name = name;
            Age = age;
        }

        public int ID { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }
    }
}