﻿/*
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2020-11-16
 * Class     : CST-247 Enterprise Computer Programming III
 * Professor : Dinesh Sthapit M.S.
 * Assignment: Activity 5
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * Inversion of Control using Unity Framework
 * 1. Constructor Injection
 * 2. Property Injection
 * 3. Method Parameter Injection
 * ---------------------------------------------------------------
 */

using Activity05.Services.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Unity;

namespace Activity05.Services.Business
{
    public class TestService : ITestService
    {
        private ILogger logger;

        [InjectionMethod]
        public void Initialize(ILogger logger)
        {
            this.logger = logger;
        }

        public void TestLogger()
        {
            logger.Info("Test Logging in TestService.TestLogger() invoked.");
        }
    }
}