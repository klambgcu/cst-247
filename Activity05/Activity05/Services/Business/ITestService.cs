﻿/*
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2020-11-16
 * Class     : CST-247 Enterprise Computer Programming III
 * Professor : Dinesh Sthapit M.S.
 * Assignment: Activity 5
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * Inversion of Control using Unity Framework
 * 1. Constructor Injection
 * 2. Property Injection
 * 3. Method Parameter Injection
 * ---------------------------------------------------------------
 */

using Activity05.Services.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Activity05.Services.Business
{
    public interface ITestService
    {
        void Initialize(ILogger logger);
        void TestLogger();
    }
}
