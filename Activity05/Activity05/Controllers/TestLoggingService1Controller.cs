﻿/*
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2020-11-16
 * Class     : CST-247 Enterprise Computer Programming III
 * Professor : Dinesh Sthapit M.S.
 * Assignment: Activity 5
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * Inversion of Control using Unity Framework
 * 1. Constructor Injection
 * 2. Property Injection
 * 3. Method Parameter Injection
 * ---------------------------------------------------------------
 */

using Activity05.Services.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Activity05.Controllers
{
    public class TestLoggingService1Controller : Controller
    {
        private readonly ILogger logger;

        public TestLoggingService1Controller(ILogger logger)
        {
            this.logger = logger;
        }

        // GET: TestLoggingService1
        public string Index()
        {
            string testResponse = "TestLoggingService1Controller: Index Test String";

            logger.Info(testResponse);
            return testResponse;
        }
    }
}