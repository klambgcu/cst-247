﻿/*
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2020-11-16
 * Class     : CST-247 Enterprise Computer Programming III
 * Professor : Dinesh Sthapit M.S.
 * Assignment: Activity 5
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * Inversion of Control using Unity Framework
 * 1. Constructor Injection
 * 2. Property Injection
 * 3. Method Parameter Injection
 * ---------------------------------------------------------------
 */

using Activity05.Services.Business;
using Activity05.Services.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Activity05.Controllers
{
    public class TestLoggingService3Controller : Controller
    {
        private readonly ILogger logger;
        private readonly ITestService service;

        public TestLoggingService3Controller(ILogger logger, ITestService service)
        {
            this.logger = logger;
            this.service = service;
        }

        // GET: TestLoggingService3
        public string Index()
        {
            string testResponse = "TestLoggingService3Controller: Index Test String";

            logger.Info(testResponse);
            service.TestLogger();

            return testResponse;
        }
    }
}