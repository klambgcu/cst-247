﻿/*
 * ---------------------------------------------------------------
 * Name      : (Part of Group Team 3)
 * Date      : 2020-11-16
 * Class     : CST-247 Enterprise Computer Programming III
 * Professor : Dinesh Staphit
 * Assignment: Milestone (REST API) 
 * ---------------------------------------------------------------
 * Description:
 * 1. API GetAllSavedGames
 * 2. API GetAllHighScores
 * Data Access Object to provides lists to populate the associated
 * Data Transfer Objects (DTO) for the APIs.
 * ---------------------------------------------------------------
 */

using MinesweeperRestAPI.Models;
using MinesweeperRestAPI.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace MinesweeperRestAPI
{
    public class MinesweeperService : IMinesweeperService
    {
        private MinesweeperDAO dao = new MinesweeperDAO();

        public GamesDTO GetAllSavedGames()
        {
            GamesDTO games = new GamesDTO();
            games.GameList = dao.GetGamesList();
            games.Code = games.GameList.Count();
            if (games.Code == 0)
            {
                games.Message = "There are currrently no games saved.";
            }
            else
            {
                games.Message = "All saved games are in the list";
            }

            return games;
        }

        public HighScoreDTO GetAllHighScores()
        {
            HighScoreDTO scores = new HighScoreDTO();
            scores.HighScoreList = dao.GetHighScoresList();
            scores.Code = scores.HighScoreList.Count();
            if (scores.Code == 0)
            {
                scores.Message = "There are currrently no highscores saved.";
            }
            else
            {
                scores.Message = "All highscores are in the list";
            }

            return scores;
        }

    }
}
