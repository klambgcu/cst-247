﻿/*
 * ---------------------------------------------------------------
 * Name      : (Part of Group Team 3)
 * Date      : 2020-11-16
 * Class     : CST-247 Enterprise Computer Programming III
 * Professor : Dinesh Staphit
 * Assignment: Milestone (REST API) 
 * ---------------------------------------------------------------
 * Description:
 * 1. API GetAllSavedGames
 * 2. API GetAllHighScores
 * Data Access Object to provides lists to populate the associated
 * Data Transfer Objects (DTO) for the APIs.
 * ---------------------------------------------------------------
 */

using MinesweeperRestAPI.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Web;

namespace MinesweeperRestAPI.Utility
{
    public class MinesweeperDAO
    {
        private string connect_string = @"Data Source=localhost\SQLEXPRESS;Initial Catalog=CST247;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

        public List<Games> GetGamesList()
        {
            List<Games> list = new List<Games>();
            string queryString = "SELECT Id, GameString FROM dbo.Games ORDER BY ID ASC";

            using (SqlConnection sqlConnection = new SqlConnection(connect_string))
            {
                using (SqlCommand sqlCommand = new SqlCommand(queryString, sqlConnection))
                {
                    try
                    {
                        sqlConnection.Open();
                        SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                        while (sqlDataReader.Read())
                        {
                            list.Add(new Games
                            {
                                ID = sqlDataReader.GetInt32(0),
                                GameString = sqlDataReader.GetString(1)
                            });
                        }
                        sqlConnection.Close();
                    }
                    catch (Exception e)
                    {
                        Debug.WriteLine("GetGamesList Database Access Failure " + e.Message);
                    }
                }
            }

            return list;
        }

        public List<HighScore> GetHighScoresList()
        {
            List<HighScore> list = new List<HighScore>();
            string queryString = "SELECT ID, UserID, LevelID, Clicks, Score, AdjustedScore FROM dbo.HighScore ORDER BY AdjustedScore ASC";

            using (SqlConnection sqlConnection = new SqlConnection(connect_string))
            {
                using (SqlCommand sqlCommand = new SqlCommand(queryString, sqlConnection))
                {
                    list.Add(new HighScore { ID = 1, UserID = 1, LevelID = 0, Clicks = 10, Score = 10, AdjustedScore = 1.0 });
                    try
                    {
                        sqlConnection.Open();
                        SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                        while (sqlDataReader.Read())
                        {
                            list.Add(new HighScore
                            {
                                ID = sqlDataReader.GetInt64(0),
                                UserID = sqlDataReader.GetInt32(1),
                                LevelID = sqlDataReader.GetInt64(2),
                                Clicks = sqlDataReader.GetInt32(3),
                                Score = sqlDataReader.GetInt32(4),
                                AdjustedScore = sqlDataReader.GetDouble(5)
                            });
                        }
                        sqlConnection.Close();
                    }
                    catch (Exception e)
                    {
                        Debug.WriteLine("GetHighScoresList Database Access Failure " + e.Message);
                    }
                }
            }

            return list;
        }
    }
}