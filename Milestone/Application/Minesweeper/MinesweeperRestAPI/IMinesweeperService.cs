﻿/*
 * ---------------------------------------------------------------
 * Name      : (Part of Group Team 3)
 * Date      : 2020-11-16
 * Class     : CST-247 Enterprise Computer Programming III
 * Professor : Dinesh Staphit
 * Assignment: Milestone (REST API) 
 * ---------------------------------------------------------------
 * Description:
 * 1. API GetAllSavedGames
 * 2. API GetAllHighScores
 * Data Access Object to provides lists to populate the associated
 * Data Transfer Objects (DTO) for the APIs.
 * ---------------------------------------------------------------
 */

using MinesweeperRestAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace MinesweeperRestAPI
{
    [ServiceContract]
    public interface IMinesweeperService
    {
        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetAllSavedGames")]
        GamesDTO GetAllSavedGames();

        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetAllHighScores")] 
        HighScoreDTO GetAllHighScores();
    }
}
