﻿/*
 * ---------------------------------------------------------------
 * Name      : (Part of Group Team 3)
 * Date      : 2020-11-16
 * Class     : CST-247 Enterprise Computer Programming III
 * Professor : Dinesh Staphit
 * Assignment: Milestone (REST API) 
 * ---------------------------------------------------------------
 * Description:
 * 1. API GetAllSavedGames
 * 2. API GetAllHighScores
 * Data Access Object to provides lists to populate the associated
 * Data Transfer Objects (DTO) for the APIs.
 * ---------------------------------------------------------------
 */

using System.Collections.Generic;
using System.Runtime.Serialization;

namespace MinesweeperRestAPI.Models
{
    [DataContract]
    public class GamesDTO
    {
        [DataMember]
        public int Code { get; set; }

        [DataMember]
        public string Message { get; set; }

        [DataMember] 
        public List<Games> GameList { get; set; }
    }
}