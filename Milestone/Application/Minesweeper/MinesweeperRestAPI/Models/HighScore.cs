﻿/*
 * ---------------------------------------------------------------
 * Name      : (Part of Group Team 3)
 * Date      : 2020-11-16
 * Class     : CST-247 Enterprise Computer Programming III
 * Professor : Dinesh Staphit
 * Assignment: Milestone (REST API) 
 * ---------------------------------------------------------------
 * Description:
 * 1. API GetAllSavedGames
 * 2. API GetAllHighScores
 * Data Access Object to provides lists to populate the associated
 * Data Transfer Objects (DTO) for the APIs.
 * ---------------------------------------------------------------
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MinesweeperRestAPI.Models
{
    public class HighScore
    {
        public long ID { get; set; }
        public int UserID { get; set; }
        public long LevelID { get; set; }
        public int Clicks { get; set; }
        public int Score { get; set; }
        public double AdjustedScore { get; set; }
    }
}