﻿using Minesweeper.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Web;

namespace Minesweeper.Utility
{
    public class GamesDAO
    {
        bool success = false;
        public bool SaveGame(GameState gameState)
        {
            String connectionString = @"Data Source=localhost\SQLEXPRESS;Initial Catalog=CST247;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

            String queryString = "INSERT INTO dbo.Games (GameString) VALUES ( @GameString )";

            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                using (SqlCommand sqlCommand = new SqlCommand(queryString, sqlConnection))
                {
                    sqlCommand.Parameters.Add("@GameString", SqlDbType.Text).Value = gameState.JSONString;

                    try
                    {
                        sqlConnection.Open();
                        sqlCommand.ExecuteNonQuery();
                        sqlConnection.Close();
                        success = true;
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Failed Conenction");
                        Debug.WriteLine(e.Message);
                    }

                }
            }
            return success;
        }

        public GameState LoadGame()
        {

            GameState gameState = new GameState(1, "");

                String connectionString = @"Data Source=localhost\SQLEXPRESS;Initial Catalog=CST247;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

                String queryString = "SELECT TOP 1 * FROM dbo.Games ORDER BY ID DESC";

                using (SqlConnection sqlConnection = new SqlConnection(connectionString))
                {
                    using (SqlCommand sqlCommand = new SqlCommand(queryString, sqlConnection))
                    {

                        try
                        {
                        sqlConnection.Open();
                        SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                        while (sqlDataReader.Read())
                        {
                            gameState.ID = sqlDataReader.GetInt32(0);
                            gameState.JSONString = sqlDataReader.GetString(1);
                        }
                            sqlConnection.Close();
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("Failed Conenction");
                            Debug.WriteLine(e.Message);
                        }

                    }
                }
                return gameState;
        }
    }
}