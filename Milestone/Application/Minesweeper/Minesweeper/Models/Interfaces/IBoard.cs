/*
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2020-09-21
 * Class     : CST-227 Enterprise Computer Programming II
 * Professor : James Shinevar
 * Assignment: Milestone 7
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Modify existing code with services and Level object
 * 2. OptionsView Form - Select Game options (Level)
 * 3. GameView Form - Contains Game board and Status Panel 
 * 4. HighScoreView Form - Contains Data Grid View and Entry Panel
 * 5. PlayerStats,PlayerStatsManager for Persistent Data
 * Objective:
 * Fully functional game play with high score capture
 * ---------------------------------------------------------------
 */

using Minesweeper.Models.Implementation;
using System.Collections.Generic;

namespace Minesweeper.Models.Interfaces
{
    public interface IBoard
    {
        int CellCount { get; set; }
        int NeutralCount { get; set; }
        int VisitedCount { get; set; }
        int BombCount { get; set; }
        int Difficulty { get; set; }
        int FlagCount { get; set; }
        int Size { get; set; }
        List<Cell> Grid { get; set; }
        Cell GetGrid(int row, int col);
    }
}