/*
 * ---------------------------------------------------------------
 * Name      : (Part of Group Team 3)
 * Date      : 2020-11-09
 * Class     : CST-247 Enterprise Computer Programming III
 * Professor : Dinesh Staphit
 * Assignment: Milestone 5
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Stores details for the game progress
 * ---------------------------------------------------------------
 */

using System;

namespace Minesweeper.Models.Interfaces
{
    public interface IGameDetail
    {
        int Clicks { get; set; }
        int Hints { get; set; }
        int Seconds { get; set; }
        DateTime StartTime { get; set; }
    }
}