﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Minesweeper.Models
{
    public class GameState
    {
        public int ID { get; set; }
        public string JSONString { get; set; }

        public GameState(int id, string jsonString)
        {
            ID = id;
            JSONString = jsonString;
        }

    }
}