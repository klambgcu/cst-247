﻿/*
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2020-11-16
 * Class     : CST-247 Enterprise Computer Programming III
 * Professor : Dinesh Sthapit M.S.
 * Assignment: Activity 5
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * Inversion of Control using Unity Framework
 * 1. Constructor Injection
 * 2. Property Injection
 * 3. Method Parameter Injection
 * ---------------------------------------------------------------
 */

using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Minesweeper.Services.Utility
{
    public class MinesweeperLogger : ILogger
    {
        // Singleton Pattern
        private static MinesweeperLogger instance;
        private static Logger logger;

        // Singleton Pattern - Private empty constructor
        private MinesweeperLogger() { }

        public static MinesweeperLogger GetInstance()
        {
            if (instance == null)
                instance = new MinesweeperLogger();

            return instance;
        }

        private Logger GetLogger(string theLogger)
        {
            if (MinesweeperLogger.logger == null)
                MinesweeperLogger.logger = LogManager.GetLogger(theLogger);

            return MinesweeperLogger.logger;
        }

        public void Debug(string message)
        {
            GetLogger("MinesweeperLoggerRules").Debug(message);
        }

        public void Error(string message)
        {
            GetLogger("MinesweeperLoggerRules").Error(message);
        }

        public void Info(string message)
        {
            GetLogger("MinesweeperLoggerRules").Info(message);
        }

        public void Warning(string message)
        {
            GetLogger("MinesweeperLoggerRules").Warn(message);
        }
    }
}