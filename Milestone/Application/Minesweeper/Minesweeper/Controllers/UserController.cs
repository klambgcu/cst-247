﻿using Minesweeper.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Minesweeper.Services.Interfaces;
using Minesweeper.Services.Implementations;
using Minesweeper.Models.Implementation;
using Minesweeper.Services.Utility;

namespace Minesweeper.Controllers
{
    public class UserController : Controller
    {
        private ILogger logger;

        public UserController(ILogger logger)
        {
            this.logger = logger;
        }

        // GET: User
        [HttpGet]
        public ActionResult Register(int id = 0)
        {
            User userModel = new User();

            return View(userModel);
        }

        // POST
        [HttpPost]
        public ActionResult Register(User userModel)
        {
            using (Minesweeper.Models.CST247Entities1 db = new Models.CST247Entities1())
            {

                // Prevent duplicate emails and usernames
                // Per professor instruction, remove FirstOrDefault, apply Any() to checks below - Thx!
                var userExists = db.Users.Where(x => x.userName == userModel.userName);
                var emailExists = db.Users.Where(x => x.email == userModel.email);

                if (userExists.Any())
                {
                    ViewBag.SuccessMessage = "User Name already exists - please select a different one!";

                    return View("Register", userModel);
                }

                if (emailExists.Any())
                {
                    ViewBag.SuccessMessage = "Email already exists - please select a different one!";

                    return View("Register", userModel);
                }

                /** TODO: HASH */
                if (ModelState.IsValid)
                {
                    #region Password Hashing
                    userModel.password = Crypto.Hash(userModel.password);
                    userModel.confirmPassword = Crypto.Hash(userModel.confirmPassword);
                    #endregion
                }


                // Save to DB
                db.Users.Add(userModel);
                try
                {
                    db.SaveChanges();
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    Exception raise = dbEx;
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            string message = string.Format("{0}:{1}",
                                validationErrors.Entry.Entity.ToString(),
                                validationError.ErrorMessage);
                            // raise a new exception nesting
                            // the current instance as InnerException
                            raise = new InvalidOperationException(message, raise);
                        }
                    }
                    throw raise;
                }

                ModelState.Clear();
                ViewBag.SuccessMessage = "Registration was Successful!";
                return View("Register", new User());
            }
        }

        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [AllowAnonymousAttribute]
        public ActionResult Login(User login, string ReturnUrl = "")
        {
            string message = "";
            try
            {
                // How we authorize user to login into the minesweeper game
                using (Minesweeper.Models.CST247Entities1 db = new Models.CST247Entities1())
                {
                    var userDetails = db.Users.Where(x => x.userName == login.userName).FirstOrDefault();
                    if (userDetails != null)
                    {
                        if (string.Compare(Crypto.Hash(login.password), userDetails.password) == 0)
                        {

                            int timeout = login.RememberMe ? 525600 : 60; // Minutes Year : Minutes - Session timeout
                            var ticket = new FormsAuthenticationTicket(login.userName, login.RememberMe, timeout);
                            string encrypted = FormsAuthentication.Encrypt(ticket);
                            var cookie = new HttpCookie(FormsAuthentication.FormsCookieName, encrypted);
                            cookie.Expires = DateTime.Now.AddMinutes(timeout);
                            cookie.HttpOnly = true;
                            Response.Cookies.Add(cookie);
                            if (Url.IsLocalUrl(ReturnUrl))
                            {
                                return Redirect(ReturnUrl);
                            }
                            else
                            {
                                // Add Game Service to the HTTP Session for visibility in the controllers
                                IGameService gameService = new GameService(new Level(), new Board(), new GameDetail());
                                Session["GameService"] = gameService;
                                Session["userID"] = userDetails.userID;

                                logger.Info("Login: User Name: " + login.userName + ", Successful login");
                                return RedirectToAction("Index", "Home");
                            }
                        }
                        else
                        {

                            message = "Invalid Information";
                            logger.Error("Login: User Name: " + login.userName + ", " + message);
                        }
                    }
                    else
                    {
                        message = "Invalid Information";
                        logger.Error("Login: User Name: " + login.userName + ", " + message);
                    }
                }
            }
            catch(Exception e)
            {
                logger.Error("Login: User Name: " + login.userName + ", " + e.Message);
            }
            ViewBag.Message = message;
            return View();
        }

        // Logout Function
        [Authorize]
        [HttpPost]
        public ActionResult LogOut()
        {
            Session.Abandon();
            Response.Cookies.Clear();
            FormsAuthentication.SignOut();
            return RedirectToAction("Login", "User");
        }

    }
}