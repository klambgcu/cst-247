﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Minesweeper.Services.Interfaces;

namespace Minesweeper.Controllers
{
    public class SecurityAuthorizationAttribute : FilterAttribute, IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationContext filterContext)
        {
            IGameService gameService = (IGameService)filterContext.HttpContext.Session["GameService"];

            if (gameService == null)
            {
                filterContext.Result = new RedirectResult("/User/Login");
            }
        }
    }
}