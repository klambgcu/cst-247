﻿using Minesweeper.Services.Interfaces;
using Minesweeper.Utility;
using Newtonsoft.Json;
using System.Web.Mvc;
using Minesweeper.Models;
using System;
using Minesweeper.Services.Implementations;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Runtime.Serialization.Json;
using System.Data.SqlClient;
using System.Linq;

namespace Minesweeper.Controllers
{
    public class MenuController : Controller
    {
        // GET: Menu
        [SecurityAuthorization]
        public ActionResult NewGame()
        {
            IGameService gameService = (IGameService)Session["GameService"];
            gameService.NewGame();

            return RedirectToAction("Index", "Home");
        }

        [SecurityAuthorization]
        public ActionResult OnSave()
        {
            // Get Game Service, convert to Json and store in database
            IGameService gameService = (IGameService)Session["GameService"];
            gameService.updateSeconds(); // Capture correct seconds elapsed
            GamesDAO gamesDAO = new GamesDAO();
            Models.GameState gameState = new Models.GameState(1, JsonConvert.SerializeObject(gameService));
            bool success = gamesDAO.SaveGame(gameState);

            return RedirectToAction("Index", "Home");
        }

        [SecurityAuthorization]
        public ActionResult OnLoad()
        {
            // Retrieve Json from database
            GamesDAO gamesDAO = new GamesDAO();
            Models.GameState gameState = gamesDAO.LoadGame();

            // Convert Json string back to object and save in session
            IGameService gameService = (GameService)JsonConvert.DeserializeObject<GameService>(gameState.JSONString);
            gameService.AdjustTimeOnRestore(); // Adjust seconds accordingly
            Session["GameService"] = gameService;

            return RedirectToAction("Index", "Home");
        }
        /** ============================================================================================================= */

        public ActionResult SaveGame()
        {
            return View();
        }
        public ActionResult RestoreGame()
        {
            return View();
        }

        [SecurityAuthorization]
        public ActionResult Options()
        {
            IGameService gameService = (IGameService)Session["GameService"];
            int newLevel = gameService.getLevel().Id + 1;
            if (newLevel > 2) newLevel = 0;
            gameService.getLevel().Id = newLevel;
            gameService.NewGame();

            return RedirectToAction("Index", "Home");
        }

        [SecurityAuthorization]
        public ActionResult Hint()
        {
            IGameService gameService = (IGameService)Session["GameService"];
            gameService.Hint();
            return RedirectToAction("CheckGameStatus", "Home");
        }

        public ActionResult HighScores()
        {
            var entities = new CST247Entities2();

            return View(entities.HighScores.OrderBy(h => h.AdjustedScore).ToList());
        }

        public ActionResult Help()
        {
            return View();
        }

        public ActionResult About()
        {
            return View();
        }

        // API - Returns Json formatted content for status of the current game
        // Must be logged into the application for information
        [SecurityAuthorization]
        public ContentResult JSON()
        {
            IGameService gameService = (IGameService)Session["GameService"];

            // Convert object to JSON string
            string json = JsonConvert.SerializeObject(gameService, Formatting.Indented);

            // ----------------------------------------------------
            // Saving for future reference
            // ----------------------------------------------------
            // Convert JSON string back to object
            // gameService = (GameService)JsonConvert.DeserializeObject<GameService>(json);
            // Convert converted object back to JSON string for return
            // string json1 = JsonConvert.SerializeObject(gameService, Formatting.Indented);

            return Content(json, "application / json");
        }

        [SecurityAuthorization]
        public ActionResult SelectEasyDifficulty()
        {
            IGameService gameService = (IGameService)Session["GameService"];
            gameService.getLevel().Id = 0;
            gameService.NewGame();

            return RedirectToAction("Index", "Home");
        }
        [SecurityAuthorization]
        public ActionResult SelectModerateDifficulty()
        {
            IGameService gameService = (IGameService)Session["GameService"];
            gameService.getLevel().Id = 1;
            gameService.NewGame();

            return RedirectToAction("Index", "Home");
        }
        [SecurityAuthorization]
        public ActionResult SelectHardDifficulty()
        {
            IGameService gameService = (IGameService)Session["GameService"];
            gameService.getLevel().Id = 2;
            gameService.NewGame();

            return RedirectToAction("Index", "Home");
        }
    }
}