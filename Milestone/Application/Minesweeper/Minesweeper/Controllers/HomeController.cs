﻿using Minesweeper.Models;
using Minesweeper.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Minesweeper.Controllers
{
    public class HomeController : Controller
    {
        [Authorize]
        [HttpGet]
        [SecurityAuthorization]

        public ActionResult Index()
        {
            IGameService gameService = (IGameService)Session["GameService"];

            return View(gameService);
        }
        // AJAX
        [HttpPost]
        public ActionResult Details()
        {
            return PartialView("_PartialGameBoard");
        }

        [SecurityAuthorization]
        public ActionResult OnLeftButtonClick(String cell)
        {
            IGameService gameService = (IGameService)Session["GameService"];

            string[] coordinates = cell.Split('|');
            int row = Int32.Parse(coordinates[0]);
            int col = Int32.Parse(coordinates[1]);

            gameService.handleLeftClick(row, col);

            return CheckGameStatus();
        }

        [SecurityAuthorization]
        public ActionResult OnRightButtonClick(String cell)
        {
            IGameService gameService = (IGameService)Session["GameService"];

            string[] coordinates = cell.Split('|');
            int row = Int32.Parse(coordinates[0]);
            int col = Int32.Parse(coordinates[1]);

            gameService.handleRightClick(row, col);

            return CheckGameStatus();
        }

        [SecurityAuthorization]
        public ActionResult CheckGameStatus()
        {
            IGameService gameService = (IGameService)Session["GameService"];

            if (gameService.GetGameState() == Services.Interfaces.GameState.GameOverLose)
            {
                return PartialView("_PartialGameOverLose", gameService);
            }
            else if (gameService.GetGameState() == Services.Interfaces.GameState.GameOverWin)
            {
                HighScore highScore = new HighScore();
                highScore.Clicks = gameService.getGameDetail().Clicks;
                highScore.Score = gameService.getGameDetail().Seconds;
                highScore.LevelID = gameService.getLevel().Id;
                highScore.AdjustedScore = highScore.Score / (gameService.getLevel().BombCount + 1.0);
                highScore.UserID = (int) Session["userID"];

                using (Minesweeper.Models.CST247Entities2 db = new Models.CST247Entities2())
                {
                    // Save to DB
                    db.HighScores.Add(highScore);

                    try
                    {
                        db.SaveChanges();
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                    {
                        Exception raise = dbEx;
                        foreach (var validationErrors in dbEx.EntityValidationErrors)
                        {
                            foreach (var validationError in validationErrors.ValidationErrors)
                            {
                                string message = string.Format("{0}:{1}",
                                    validationErrors.Entry.Entity.ToString(),
                                    validationError.ErrorMessage);
                                // raise a new exception nesting
                                // the current instance as InnerException
                                raise = new InvalidOperationException(message, raise);
                            }
                        }
                        throw raise;
                    }
                }

                return PartialView("_PartialGameOverWin", gameService);
            }
            else
                return View("Index", gameService);
        }

        [SecurityAuthorization]
        public ActionResult MaxTimeElapsed()
        {
            IGameService gameService = (IGameService)Session["GameService"];

            // Inform game service that game is lost
            gameService.GameOverTimeExceeded();

            return View("GameOverLose", gameService);
        }
    }
}